
(function($){

	// component form
	Vue.component('Editor', {
		template: '#form-template',
		props: ['value', 'items'],
		methods: {
			onSubmit (event) {
				event.preventDefault();
				console.log(this.value);
				this.items.push(this.value);
				$('#exampleModal').modal('toggle');
				console.log(this.items);
			},
			onChangeFile () {
				this.form.img = '';
			}
		}
	});


	new Vue({
		el: '#app',
		data: {
			items: [
				{
					img: 'https://via.placeholder.com/250',
					title: 'Titre 1',
					price: 120,
					description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur officiis deleniti minima dolorum doloribus commodi reprehenderit ut ducimus dolore repellendus nulla quas, magni distinctio eos, quae asperiores, ratione necessitatibus laudantium.'
				},
				{
					img: 'https://via.placeholder.com/250',
					title: 'Titre 1',
					price: 120,
					description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur officiis deleniti minima dolorum doloribus commodi reprehenderit ut ducimus dolore repellendus nulla quas, magni distinctio eos, quae asperiores, ratione necessitatibus laudantium.'
				},
				{
					img: 'https://via.placeholder.com/250',
					title: 'Titre 1',
					price: 120,
					description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur officiis deleniti minima dolorum doloribus commodi reprehenderit ut ducimus dolore repellendus nulla quas, magni distinctio eos, quae asperiores, ratione necessitatibus laudantium.'
				},
				{
					img: 'https://via.placeholder.com/250',
					title: 'Titre 1',
					price: 120,
					description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur officiis deleniti minima dolorum doloribus commodi reprehenderit ut ducimus dolore repellendus nulla quas, magni distinctio eos, quae asperiores, ratione necessitatibus laudantium.'
				}
			],
			value: {
				title: '',
				price: 0,
				description: '',
			}
		},
		methods: {
			onClick () {
				this.showed = true;
			},
			onEdit (key) {
				this.value = this.items[key];
				$('[data-toggle="modal"]').trigger('click');
			}
		}
	});
}(jQuery));